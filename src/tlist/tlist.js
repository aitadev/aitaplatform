var express = require( 'express' );
var router = new express.Router();


function getTlist(req, res){
    var testOutput = {};
    testOutput.Result = "Success";
    res.status(200).send(JSON.stringify(testOutput));
}


function getTlistMock(req, res){
    var tList = [];
    var tCategory = ["under12 boys", "under14 boys", "under12 girls", "under 14 girls", "Men 50k", "Women 50k", "Men 1L", "Women 1L", "Men 2.5L", "Men 5L"];
    for(i=0;i<1;i++) {
    	var tournament= new Object();
	tournament.tid = i.toString();
	tournament.tname = "Aita Powerpak " + tCategory[i];
	tournament.startDate = new Date(2018, 7, 12, 10, 33, 30, 0);
	tournament.tcategory = "Aita " + tCategory[i];
	tList.push(tournament);
    }
    var jsonObj = {};
    jsonObj.items = tList;
    res.status(200).send(JSON.stringify(jsonObj));
}



function getMlistMock(req, res){
    console.log("Got request for: ", req.params.status);
    var mList = [];
    var tCategory = ["under12 boys", "under14 boys", "under12 girls", "under 14 girls", "Men 50k", "Women 50k", "Men 1L", "Women 1L", "Men 2.5L", "Men 5L"];
    for(i=0;i<10;i++) {
    	var match = new Object();
	match.tid = req.params.tid;
	match.mid = "R128M" + i.toString();
	match.tname = "AITA PowerPak Tennis";
	var player1 = new Object();
	player1.name="sheera";
	player1.aitaId="882339";
	player1.pointScore="15";
	player1.gameScore="3";
	player1.setScore="1";
	
	var player2 = new Object();
	player2.name="ravi";
	player2.aitaId="882390";
	player2.pointScore="30";
	player2.gameScore="5";
	player2.setScore="1";
	match.player1 = player1;
	match.player2 = player2;
	mList.push(match);
    }
    var jsonObj = {};
    jsonObj.items = mList;
    res.status(200).send(JSON.stringify(jsonObj));
}

function getStatListMock(req, res){
    console.log("Got request for stats");
    var jsonObj = {};
    
    jsonObj.tid = req.params.tid;
    jsonObj.mid = req.params.mid;
    jsonObj.tname = "AITA PowerPak Tennis";
    jsonObj.status = "InProgress";
    jsonObj.servingPlayer = "player1";
    
    var player1 = new Object();
    player1.name="player1";
    player1.aitaId="882339";
    
    var sets=[];
    var set1=new Object();
    set1.gameScore="5";
    set1.result="L";

    var set2=new Object();
    set2.gameScore="4";
    set2.pointScore="15";

    var stats=new Object();
    stats.aces="14";
    stats.winners="1";
    stats.doubleFaults="0"
    stats.errors="19";
    stats.firstServeWon="78%";
    stats.secondServeWon="40%";
    stats.breakPointsConverted="1/6";

    sets.push(set1);
    sets.push(set2);
    player1.sets = sets;
    player1.stats = stats;
    jsonObj.player1 = player1;

    var stats=new Object();
    stats.aces="18";
    stats.winners="5";
    stats.doubleFaults="0"
    stats.errors="5";
    stats.firstServeWon="80%";



    var player2 = new Object();
    player2.name="player2";
    player2.aitaId="444332";
    
    var sets2=[];
    var set1=new Object();
    set1.gameScore="7";
    set1.result="W";

    var set2=new Object();
    set2.gameScore="2";
    set2.pointScore="30";

    sets2.push(set1);
    sets2.push(set2);
    player2.sets = sets2;
    player2.stats=stats;
    jsonObj.player1 = player1;
    jsonObj.player2 = player2;


    res.status(200).send(JSON.stringify(jsonObj));
}

router.get('/tdate/:tdate', getTlist);
router.get('/mock/', getTlistMock);
router.get('/mock/tlist/:tid/mlist/:status', getMlistMock);
router.get('/mock/tlist/:tid/mlist/:mid/stats', getStatListMock);

exports.router = router;
