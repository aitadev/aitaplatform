var express = require( 'express' ),
    config = require( 'config'),
    tlist = require( './tlist/tlist' );

var MODULE_ID = '[AITA_SERVER]';

var LISTEN_PORT = config.server.port;

var app = express();

app.use( '/tlist', tlist.router);

function start(callback) {

    server = app.listen(LISTEN_PORT, function() {
	
	console.log('Listening on port '+ LISTEN_PORT);

    });

}


if (require.main === module) {
	// started as a script
	start();
} else {
	// started by require().
    exports.start = start;
    exports.stop = stop;
}
